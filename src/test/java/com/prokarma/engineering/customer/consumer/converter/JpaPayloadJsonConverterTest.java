package com.prokarma.engineering.customer.consumer.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.prokarma.engineering.customer.consumer.convert.JpaPayloadJsonConverter;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

class JpaPayloadJsonConverterTest {
  private JpaPayloadJsonConverter converter = new JpaPayloadJsonConverter();

  @Test
  void convertToDatabaseColumnTest() {

    String actualResult = converter.convertToDatabaseColumn(getCustomerData());
    String expectedResult =
        "{\"customerNumber\":\"******0001\",\"firstName\":\"durgaprasad\",\"lastName\":\"durgaprasad\",\"birthdate\":\"**-**-1994\",\"country\":\"india\",\"countryCode\":\"IN\",\"mobileNumber\":\"5555551216\",\"email\":\"abc****@gmail.com\",\"customerStatus\":\"Restored\",\"address\":\"address\",\"activityId\":null,\"transactionId\":null}";
    assertEquals(expectedResult, actualResult);
  }

  @Test
  void convertToEntityAttributeTest() {
    String customerStringInput =
        "{\"customerNumber\":\"******0001\",\"firstName\":\"durgaprasad\",\"lastName\":\"durgaprasad\",\"birthdate\":\"**-**-1994\",\"country\":\"india\",\"countryCode\":\"IN\",\"mobileNumber\":\"5555551216\",\"email\":\"abc****@gmail.com\",\"customerStatus\":\"Restored\",\"address\":\"address\",\"activityId\":null,\"transactionId\":null}";

    KafkaRequestPayload requestPayload = converter.convertToEntityAttribute(customerStringInput);
    assertEquals("******0001", requestPayload.getCustomerNumber());
    assertEquals("abc****@gmail.com", requestPayload.getEmail());
    assertEquals("**-**-1994", requestPayload.getBirthdate());
  }

  private KafkaRequestPayload getCustomerData() {

    KafkaRequestPayload customerRequest = new KafkaRequestPayload();
    customerRequest.setCustomerNumber("******0001");
    customerRequest.setFirstName("durgaprasad");
    customerRequest.setLastName("durgaprasad");
    customerRequest.setBirthdate("**-**-1994");
    customerRequest.setCountry("india");
    customerRequest.setCountryCode("IN");
    customerRequest.setMobileNumber("5555551216");
    customerRequest.setEmail("abc****@gmail.com");
    customerRequest.setCustomerStatus("Restored");
    customerRequest.setAddress("address");

    return customerRequest;
  }
}

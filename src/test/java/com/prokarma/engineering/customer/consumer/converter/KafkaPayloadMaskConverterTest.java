package com.prokarma.engineering.customer.consumer.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.prokarma.engineering.customer.consumer.convert.KafkaPayloadMaskConverter;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

class KafkaPayloadMaskConverterTest {
  private KafkaPayloadMaskConverter converter = new KafkaPayloadMaskConverter();

  @Test
  void testConvert() {

    KafkaRequestPayload maskRequest = converter.convert(getCustomerData());
    assertEquals("******0001", maskRequest.getCustomerNumber());
    assertEquals("abc****@gmail.com", maskRequest.getEmail());
    assertEquals("**-**-1994", maskRequest.getBirthdate());
  }

  private KafkaRequestPayload getCustomerData() {

    KafkaRequestPayload customerRequest = new KafkaRequestPayload();
    customerRequest.setCustomerNumber("C000000001");
    customerRequest.setFirstName("durgaprasad");
    customerRequest.setLastName("durgaprasad");
    customerRequest.setBirthdate("27-11-1994");
    customerRequest.setCountry("india");
    customerRequest.setCountryCode("IN");
    customerRequest.setMobileNumber("5555551216");
    customerRequest.setEmail("abcdefg@gmail.com");
    customerRequest.setCustomerStatus("RESTORED");
    customerRequest.setAddress("address");

    return customerRequest;
  }
}

package com.prokarma.engineering.customer.consumer.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

@ExtendWith(SpringExtension.class)
@SpringBootTest()
@EmbeddedKafka
class KafkaConsumerServiceImplTest {

  private static String TOPIC_NAME = "TestTopic";

  @Autowired private KafkaConusmerServiceImpl kafkaMessageProducerService;

  private KafkaMessageListenerContainer<String, KafkaRequestPayload> container;

  private BlockingQueue<ConsumerRecord<String, String>> consumerRecords;

  public EmbeddedKafkaRule embeddedKafka = new EmbeddedKafkaRule(1, true, TOPIC_NAME);

  @BeforeAll
  public void setUp() {
    consumerRecords = new LinkedBlockingQueue<>();

    ContainerProperties containerProperties = new ContainerProperties(TOPIC_NAME);

    Map<String, Object> consumerProperties =
        KafkaTestUtils.consumerProps("consumer", "false", embeddedKafka.getEmbeddedKafka());

    DefaultKafkaConsumerFactory<String, KafkaRequestPayload> consumer =
        new DefaultKafkaConsumerFactory<>(consumerProperties);

    container = new KafkaMessageListenerContainer<>(consumer, containerProperties);
    container.setupMessageListener(
        (MessageListener<String, String>)
            record -> {
              consumerRecords.add(record);
            });
    container.start();

    ContainerTestUtils.waitForAssignment(
        container, embeddedKafka.getEmbeddedKafka().getPartitionsPerTopic());
  }

  @AfterAll
  public void tearDown() {
    container.stop();
  }

  @Test
  void testcustomerPayloadListener() throws Exception {
    Map<String, Object> configs =
        new HashMap<>(KafkaTestUtils.producerProps(embeddedKafka.getEmbeddedKafka()));
    Producer<String, String> producer =
        new DefaultKafkaProducerFactory<>(configs, new StringSerializer(), new StringSerializer())
            .createProducer();
    String payload =
        "{\"customerNumber\":\"******0001\",\"firstName\":\"durgaprasad\",\"lastName\":\"durgaprasad\",\"birthdate\":\"**-**-1994\",\"country\":\"india\",\"countryCode\":\"IN\",\"mobileNumber\":\"5555551216\",\"email\":\"abc****@gmail.com\",\"customerStatus\":\"Restored\",\"address\":\"address\",\"activityId\":null,\"transactionId\":null}";
    producer.send(new ProducerRecord<>(TOPIC_NAME, "producer-key", payload));
    producer.flush();

    ConsumerRecord<String, String> singleRecord = consumerRecords.poll(100, TimeUnit.MILLISECONDS);
    assertThat(singleRecord).isNotNull();
    assertThat(singleRecord.key()).isEqualTo("producer-key");
    assertThat(singleRecord.value())
        .isEqualTo(
            "{\"customerNumber\":\"******0001\",\"firstName\":\"durgaprasad\",\"lastName\":\"durgaprasad\",\"birthdate\":\"**-**-1994\",\"country\":\"india\",\"countryCode\":\"IN\",\"mobileNumber\":\"5555551216\",\"email\":\"abc****@gmail.com\",\"customerStatus\":\"Restored\",\"address\":\"address\",\"activityId\":null,\"transactionId\":null}");
  }
}

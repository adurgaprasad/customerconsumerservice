package com.prokarma.engineering.customer.consumer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.prokarma.engineering.customer.consumer.entity.AuditLog;
import com.prokarma.engineering.customer.consumer.repository.AuditLogRepository;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

@ExtendWith(SpringExtension.class)
class CustomerConsumerImplTest {
  @InjectMocks KafkaRequestPayload kafkaRequestPayload;

  @Mock private AuditLogRepository auditLogRepository;

  @Test
  void testPersistCustomerPayload() throws JsonProcessingException {
    CustomerConsumerServiceImpl mockCustomerConsumerService =
        mock(CustomerConsumerServiceImpl.class);
    when(auditLogRepository.save(Mockito.any(AuditLog.class))).thenReturn(getAuditLog());
    doAnswer(
            (i) -> {
              KafkaRequestPayload customerRequest = i.getArgument(0);
              assertEquals("******0001", customerRequest.getCustomerNumber());
              return null;
            })
        .when(mockCustomerConsumerService)
        .persistCustomerPayload(getCustomerData());
    mockCustomerConsumerService.persistCustomerPayload(getCustomerData());
  }

  private KafkaRequestPayload getCustomerData() {

    KafkaRequestPayload customerRequest = new KafkaRequestPayload();
    customerRequest.setCustomerNumber("******0001");
    customerRequest.setFirstName("durgaprasad");
    customerRequest.setLastName("durgaprasad");
    customerRequest.setBirthdate("**-**-1994");
    customerRequest.setCountry("india");
    customerRequest.setCountryCode("IN");
    customerRequest.setMobileNumber("5555551216");
    customerRequest.setEmail("abc****@gmail.com");
    customerRequest.setCustomerStatus("Restored");
    customerRequest.setAddress("address");

    return customerRequest;
  }

  private AuditLog getAuditLog() {
    AuditLog auditLog = new AuditLog();
    auditLog.setCustomerNumber("******0001");
    auditLog.setPayload(getCustomerData());
    return auditLog;
  }
}

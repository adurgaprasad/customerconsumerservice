package com.prokarma.engineering.customer.publisher.domain;

public class KafkaRequestPayload {

  private String customerNumber;

  private String firstName;

  private String lastName;

  private String birthdate;

  private String country;

  private String countryCode;

  private String mobileNumber;

  private String email;

  private String customerStatus;

  private String address;

  private String activityId;

  private String transactionId;

  public String getActivityId() {
    return activityId;
  }

  public void setActivityId(String activityId) {
    this.activityId = activityId;
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public String getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getBirthdate() {
    return birthdate;
  }

  public void setBirthdate(String birthdate) {
    this.birthdate = birthdate;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCustomerStatus() {
    return customerStatus;
  }

  public void setCustomerStatus(String customerStatus) {
    this.customerStatus = customerStatus;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Override
  public String toString() {
    return "KafkaRequestPayload [customerNumber="
        + customerNumber
        + ", firstName="
        + firstName
        + ", lastName="
        + lastName
        + ", birthdate="
        + birthdate
        + ", country="
        + country
        + ", countryCode="
        + countryCode
        + ", mobileNumber="
        + mobileNumber
        + ", email="
        + email
        + ", customerStatus="
        + customerStatus
        + ", address="
        + address
        + "]";
  }
}

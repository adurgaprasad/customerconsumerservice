package com.prokarma.engineering.customer.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prokarma.engineering.customer.consumer.entity.AuditLog;

public interface AuditLogRepository extends JpaRepository<AuditLog, Long> {}

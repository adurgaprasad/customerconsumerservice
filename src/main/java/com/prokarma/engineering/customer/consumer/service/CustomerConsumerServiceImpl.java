package com.prokarma.engineering.customer.consumer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.prokarma.engineering.customer.consumer.entity.AuditLog;
import com.prokarma.engineering.customer.consumer.exception.ConsumerApplicationFailureException;
import com.prokarma.engineering.customer.consumer.repository.AuditLogRepository;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

@Service
public class CustomerConsumerServiceImpl implements CustomerConsumerService {

  Logger logger = LoggerFactory.getLogger(CustomerConsumerServiceImpl.class);
  @Autowired private AuditLogRepository auditLogRepo;

  @Override
  public void persistCustomerPayload(KafkaRequestPayload customerPayload) {

    AuditLog auditLog = new AuditLog();
    auditLog.setCustomerNumber(customerPayload.getCustomerNumber());
    auditLog.setPayload(customerPayload);
    try {
      auditLogRepo.save(auditLog);
    } catch (DataAccessException ex) {
      new ConsumerApplicationFailureException("Data save failed to the audit table");
    }

    logger.info("Customer data successfully saved in AuditLog Table");
  }
}

package com.prokarma.engineering.customer.consumer.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.prokarma.engineering.customer.consumer.exception.ConsumerApplicationFailureException;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

public class ObjectMapperUtil {

  private ObjectMapperUtil() {
    throw new AssertionError("No Object Creation");
  }

  public static String convertObjectToJson(Object object) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(object);
    } catch (JsonProcessingException ex) {
      throw new ConsumerApplicationFailureException("Json processing exception" + ex);
    }
  }

  public static KafkaRequestPayload convertJsonToObject(String dbData) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.readValue(dbData, KafkaRequestPayload.class);
    } catch (JsonProcessingException ex) {
      throw new ConsumerApplicationFailureException("Json processing exception" + ex);
    }
  }
}

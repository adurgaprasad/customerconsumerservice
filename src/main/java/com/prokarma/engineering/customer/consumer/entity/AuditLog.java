package com.prokarma.engineering.customer.consumer.entity;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.prokarma.engineering.customer.consumer.convert.JpaPayloadJsonConverter;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

@Entity
@Table(name = "AUDIT_LOG")
public class AuditLog {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;

  @Column(name = "CUSTOMER_NUMBER")
  private String customerNumber;

  @Column(name = "payload", length = 100000)
  @Convert(converter = JpaPayloadJsonConverter.class)
  private KafkaRequestPayload payload;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public KafkaRequestPayload getPayload() {
    return payload;
  }

  public void setPayload(KafkaRequestPayload payload) {
    this.payload = payload;
  }
}

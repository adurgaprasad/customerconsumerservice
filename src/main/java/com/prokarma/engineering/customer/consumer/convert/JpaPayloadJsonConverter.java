package com.prokarma.engineering.customer.consumer.convert;

import javax.persistence.AttributeConverter;

import com.prokarma.engineering.customer.consumer.util.ObjectMapperUtil;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

public class JpaPayloadJsonConverter implements AttributeConverter<KafkaRequestPayload, String> {

  @Override
  public String convertToDatabaseColumn(KafkaRequestPayload attribute) {
    return ObjectMapperUtil.convertObjectToJson(attribute);
  }

  @Override
  public KafkaRequestPayload convertToEntityAttribute(String dbData) {
    return ObjectMapperUtil.convertJsonToObject(dbData);
  }
}

package com.prokarma.engineering.customer.consumer.service;

import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

public interface CustomerConsumerService {
  public void persistCustomerPayload(KafkaRequestPayload customerPayload);
}

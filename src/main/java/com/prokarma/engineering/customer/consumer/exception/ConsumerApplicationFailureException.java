package com.prokarma.engineering.customer.consumer.exception;

public class ConsumerApplicationFailureException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public ConsumerApplicationFailureException(Throwable ex) {
    super(ex);
  }

  public ConsumerApplicationFailureException(String message) {
    super(message);
  }
}

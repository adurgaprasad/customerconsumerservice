package com.prokarma.engineering.customer.consumer.constant;

import org.springframework.stereotype.Component;

@Component
public class CustomerConsumerConstants {
  public static final String BIRTHDATE_REGEX = "\\d(?=(?:\\D*\\d){4})";
  public static final String CUSTOMERNUMBER_REGEX = "\\w(?=\\w{4})";
  public static final String EMAIL_REGEX = "(?<=.{3}).(?=.*@)";
  public static final String TOPIC = "${kafka.topic}";
  public static final String GROUPID = "${kafka.group-id}";

  private CustomerConsumerConstants() {
    super();
  }

  public static String getBirthdateRegex() {
    return BIRTHDATE_REGEX;
  }

  public static String getCustomernumberRegex() {
    return CUSTOMERNUMBER_REGEX;
  }

  public static String getEmailRegex() {
    return EMAIL_REGEX;
  }

  public static String getTopic() {
    return TOPIC;
  }

  public static String getGroupid() {
    return GROUPID;
  }
}

package com.prokarma.engineering.customer.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prokarma.engineering.customer.consumer.entity.ErrorLog;

public interface ErrorLogRepository extends JpaRepository<ErrorLog, Long> {}

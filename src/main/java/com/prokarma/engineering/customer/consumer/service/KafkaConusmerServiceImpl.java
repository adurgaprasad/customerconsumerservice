package com.prokarma.engineering.customer.consumer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.prokarma.engineering.customer.consumer.constant.CustomerConsumerConstants;
import com.prokarma.engineering.customer.consumer.convert.Converter;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

@Service
public class KafkaConusmerServiceImpl {

  Logger logger = LoggerFactory.getLogger(KafkaConusmerServiceImpl.class);

  @Autowired CustomerConsumerService customerConsumerService;
  @Autowired Converter maskConverter;

  @KafkaListener(
      topics = CustomerConsumerConstants.TOPIC,
      groupId = CustomerConsumerConstants.GROUPID)
  public void customerPayloadListener(KafkaRequestPayload customerPayload) {
    KafkaRequestPayload maskedCustomerPayload = maskConverter.convert(customerPayload);
    logger.info("The Customer data consumed from topic is {}", maskedCustomerPayload);
    customerConsumerService.persistCustomerPayload(maskedCustomerPayload);
  }
}

package com.prokarma.engineering.customer.consumer.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prokarma.engineering.customer.consumer.entity.ErrorLog;
import com.prokarma.engineering.customer.consumer.repository.ErrorLogRepository;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

@Component
@Aspect
public class ErrorLogAspect {
  Logger logger = LoggerFactory.getLogger(ErrorLogAspect.class);

  @Autowired private ErrorLogRepository errorLogRepo;

  @AfterThrowing(
      pointcut =
          "execution(* com.prokarma.engineering.customer.consumer.service.CustomerConsumerServiceImpl.persistCustomerPayload(..))",
      throwing = "ex")
  public void afterThrowingAdvice(JoinPoint joinPoint, Throwable ex) {
    KafkaRequestPayload requestPaylod = (KafkaRequestPayload) joinPoint.getArgs()[0];

    ErrorLog errorLog = new ErrorLog();
    errorLog.setPayload(requestPaylod);
    errorLog.setErrorType(ex.getClass().getName());
    errorLog.setErrorDescription(ex.getMessage());
    errorLogRepo.save(errorLog);
    logger.info("ErrorLog entry {} is persisted", errorLog);
  }
}

package com.prokarma.engineering.customer.consumer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.listener.LoggingErrorHandler;

@Configuration
public class KafkaConsumerConfig {
  @Bean
  public LoggingErrorHandler errorHandler() {
    return new LoggingErrorHandler();
  }
}

package com.prokarma.engineering.customer.consumer.convert;

import org.springframework.stereotype.Component;

import com.prokarma.engineering.customer.consumer.constant.CustomerConsumerConstants;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

@Component
public class KafkaPayloadMaskConverter implements Converter {

  @Override
  public KafkaRequestPayload convert(KafkaRequestPayload request) {
    KafkaRequestPayload maskCustomerRequest = new KafkaRequestPayload();
    maskCustomerRequest.setCustomerNumber(
        request
            .getCustomerNumber()
            .replaceAll(CustomerConsumerConstants.CUSTOMERNUMBER_REGEX, "*"));
    maskCustomerRequest.setFirstName(request.getFirstName());
    maskCustomerRequest.setLastName(request.getLastName());
    maskCustomerRequest.setBirthdate(
        request.getBirthdate().replaceAll(CustomerConsumerConstants.BIRTHDATE_REGEX, "*"));
    maskCustomerRequest.setCountry(request.getCountry());
    maskCustomerRequest.setCountryCode(request.getCountryCode());
    maskCustomerRequest.setMobileNumber(request.getMobileNumber());
    maskCustomerRequest.setEmail(
        request.getEmail().replaceAll(CustomerConsumerConstants.EMAIL_REGEX, "*"));
    maskCustomerRequest.setCustomerStatus(request.getCustomerStatus());
    maskCustomerRequest.setAddress(request.getAddress());

    return maskCustomerRequest;
  }
}

package com.prokarma.engineering.customer.consumer.convert;

import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

public interface Converter {
  public KafkaRequestPayload convert(KafkaRequestPayload request);
}

package com.prokarma.engineering.customer.consumer.entity;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.prokarma.engineering.customer.consumer.convert.JpaPayloadJsonConverter;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

@Entity
@Table(name = "ERROR_LOG")
public class ErrorLog {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;

  @Column(name = "ERROR_TYPE")
  private String errorType;

  @Column(name = "ERROR_DESCRIPTION")
  private String errorDescription;

  @Column(name = "PAYLOAD", length = 100000)
  @Convert(converter = JpaPayloadJsonConverter.class)
  private KafkaRequestPayload payload;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getErrorType() {
    return errorType;
  }

  public void setErrorType(String errorType) {
    this.errorType = errorType;
  }

  public String getErrorDescription() {
    return errorDescription;
  }

  public void setErrorDescription(String errorDescription) {
    this.errorDescription = errorDescription;
  }

  public KafkaRequestPayload getPayload() {
    return payload;
  }

  public void setPayload(KafkaRequestPayload payload) {
    this.payload = payload;
  }
}
